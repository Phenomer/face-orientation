#!/usr/bin/python3
#-*- coding: utf-8 -*-

import math
import sys
import os
import time
import socket
import json
import random
from pathlib import Path
import dlib
import svgwriter

IMG_DIR     = '.'
IMG_NAME    = './static/sample.jpg'
SVG_NAME    = './static/sample.svg'
LEARN_DATA  = '/usr/share/dlib/shape_predictor_68_face_landmarks.dat'
WIDTH       = 320
HEIGHT      = 240
SVGOPT      = "stroke=\"red\" stroke-width=\"3\""

detector    = dlib.get_frontal_face_detector()
predictor   = dlib.shape_predictor(LEARN_DATA)

def distance(p1, p2):
    return math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2)

def image_processor():
    # 一番最後に生成されたwebcam-*.jpgを探し、static/sample.jpgにrename
    samples = list(Path('.').glob('webcam-*.jpg'))
    if len(samples) == 0:
        return {'x': 0, 'y': 0, 'z': 0, 'orientation': 0}
    sorted(samples)[-1].replace(IMG_NAME)

    img  = dlib.load_rgb_image(IMG_NAME)
    dets = detector(img, 1)
    svg  = svgwriter.SvgWriter(SVG_NAME, WIDTH, HEIGHT, SVGOPT)
    c, x, y = 0, 0, 0
    left, center, right = None, None, None
    for k, d in enumerate(dets):
        shape = predictor(img, d)
        c += 1
        # 顔の中心(30)
        x += shape.part(30).x
        y += shape.part(30).y
        # 口の左(48), 中央(62), 右(54)
        left   = shape.part(48)
        center = shape.part(62)
        right  = shape.part(54)
        svg.update(d, shape)
    svg.close()

    if (c > 0):
        leftweight = distance(left, center)
        rightweight = distance(center, right)
        orientation = leftweight - rightweight
        xabs, yabs = WIDTH / 2, HEIGHT / 2
        xpos, ypos = (x / c - xabs) / xabs, (y / c - yabs) / yabs
        return {'x': xpos * -1, 'y': ypos, 'z': 0, 'orientation': orientation}
    else:
        return {'x': 0, 'y': 0, 'z': 0, 'orientation': 0}

from flask import Flask, render_template
app  = Flask(__name__)
        
@app.route('/')
def root():
    return render_template('index.html')

@app.route('/face.json')
def face_json():
    return json.dumps(image_processor()).encode("UTF-8")
