function update(){
    const threshold = 3;
    
    fetch('/face.json')
	.then((res)=>{
	    if (res.ok){return res.json();}
	    throw new Error('network error');
	}).then((json)=>{
	    console.log(json);
	    document.getElementById("status_x").innerHTML = json['x'];
	    document.getElementById("status_y").innerHTML = json['y'];
	    document.getElementById("status_z").innerHTML = json['z'];
	    document.getElementById("status_o").innerHTML = json['orientation'];
	    if (json['orientation'] < -threshold){
		document.getElementById("status_text").innerHTML = "===>>>";

	    }else if(json['orientation'] > threshold){
		document.getElementById("status_text").innerHTML = "<<<===";
	    } else{
		document.getElementById("status_text").innerHTML = "======";
	    }
	    document.getElementById("sample_svg").src = "/static/sample.svg?time=" + Date.now()
	    document.getElementById("sample_jpg").src = "/static/sample.jpg?time=" + Date.now()
	})
}

function init(){
    setInterval(update, 500);
}
window.addEventListener('load', init);
