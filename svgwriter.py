#-*- coding: utf-8 -*-

class SvgWriter:
    def __init__(self, svgpath, width, height, svgopt=""):
        self.width   = width
        self.height  = height
        self.svgopt  = svgopt
        self.svgfile = open(svgpath, 'w')
        self.write_header()

    def write_header(self):
        self.svgfile.write("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" "
                           "\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
                           "<svg width=\"{}\" height=\"{}\" "
                           "xmlns=\"http://www.w3.org/2000/svg\" "
                           "xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n"
                           .format(self.width, self.height))

    def close(self):
        self.svgfile.write("</svg>")
        self.svgfile.close

    def update(self, detector, shape):
        self.svgfile.write("<rect x=\"{}\" y=\"{}\" width=\"{}\" height=\"{}\"/>\n"
                           .format(detector.left(), detector.top(),
                                   detector.right() - detector.left(),
                                   detector.bottom() - detector.top(),
                                   self.svgopt))
        self.svgfile.write("<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" {}/>\n"
                           .format(shape.part(36).x, shape.part(36).y,
                                   shape.part(39).x, shape.part(39).y,
                                   self.svgopt))
        self.svgfile.write("<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" {}/>\n"
                           .format(shape.part(42).x, shape.part(42).y,
                                   shape.part(45).x, shape.part(45).y,
                                   self.svgopt))
        self.svgfile.write("<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" {}/>\n"
                           .format(shape.part(30).x, shape.part(30).y,
                                   shape.part(33).x, shape.part(33).y,
                                   self.svgopt))
        self.svgfile.write("<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" {}/>\n"
                           .format(shape.part(51).x, shape.part(51).y,
                                   shape.part(57).x, shape.part(57).y,
                                   self.svgopt))
        self.svgfile.write("<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" {}/>\n"
                           .format(shape.part(48).x, shape.part(48).y,
                                   shape.part(54).x, shape.part(54).y,
                                   self.svgopt))
        self.svgfile.write("<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" {}/>\n"
                           .format(shape.part(31).x, shape.part(31).y,
                                   shape.part(35).x, shape.part(35).y,
                                   self.svgopt))
