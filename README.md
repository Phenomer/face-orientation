# これはなに?
顔の左右の向きを大雑把に検知してWebページ上に表示するシステムです。


# つかいかた
## 必要なソフトウェアのインストール

python3とlibdlibをインストールします。
(ubuntu-18.04の場合はlibdlib18)

- apt install python3 python3-pip libdlib19 libdlib19-dev libdlib19-data gstreamer1.0-tools

## 必要なライブラリのインストール
- pip install Flask
- pip install numpy
- pip install dlib

## ハードウェアの準備
- Webカメラを接続
- webcam.sh を実行
  (異常終了する場合は、webcam.sh内パラメータを修正)

## Webサービスの開始
- ./server.sh
- ブラウザから http://127.0.0.1:5000 にアクセス


# 注意点
ワーキングディレクトリ内に画像ファイルを大量に書き出すので、
フラッシュメモリ上などで利用する場合は注意してください。
(tmpfs上などで動作させることを推奨します。)
